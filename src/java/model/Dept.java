package model;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import util.Annotation;
import util.ModelView;

@Annotation(url="dept")
public class Dept {
    int idDept;
    String nomDept;
    String loc;

    public Dept(int idDept, String nomDept, String loc) {
        this.idDept = idDept;
        this.nomDept = nomDept;
        this.loc = loc;
    }
    
    public Dept() {
    }

    public Dept(String nomDept, String loc) {
        this.nomDept = nomDept;
        this.loc = loc;
    }

    public int getIdDept() {
        return idDept;
    }

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }
    
    @Annotation(url="listeDept")
    public ModelView ListeDept() throws SQLException{
       
      
        ModelView mv=new ModelView();
        ArrayList<Dept> lE=new ArrayList<>();
        Dept e=new Dept(1,"RH","Analakely");
        Dept ee=new Dept(2,"Securite","Faravohitra");
        lE.add(e);
        lE.add(ee);
        HashMap<String,Object> data=new HashMap<String,Object>();
        data.put("liste",lE);
        mv.setUrl("/listeDept.jsp");
        mv.setListe(data);
        
    return mv;
        
    }
    @Annotation(url="ajoutDept")
    public ModelView AjoutDept(){
        ModelView mv = new ModelView();
        mv.setUrl("/result.jsp");
        HashMap<String,Object> hm = new HashMap<>();
        hm.put("dept", this);
        mv.setListe(hm);
        return mv;
    }
    
}